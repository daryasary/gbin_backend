import os

DEFAULT_TEMPLATE = 'appearances/default'


def get_user_template(user):
    """Make a query call on db and find user selected template"""
    # TODO: User template model does not designed yet
    return DEFAULT_TEMPLATE


def get_complete_template_path_for_user(user, template_page):
    """Check if user had purchased and selected any custom template and map
    selected page to user preferred template, if user has no template or
    selected does not exists in template return default template path"""
    template = get_user_template(user)
    if template_page.startswith('/'):
        template_page = template_page[1:]
    if not os.path.exists(template + template_page):
        return os.path.join(template, template_page)
    return os.path.join(DEFAULT_TEMPLATE, template_page)
