from django.core import validators
from django.utils.translation import ugettext_lazy as _


def mobile_number_validator(*args):
    return validators.RegexValidator(
        regex=r'^989[0-3,9]\d{8}$',
        message=_('Enter a valid mobile number.'),
        code='invalid'
    )
