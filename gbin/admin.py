from django.contrib import admin
from django.contrib.admin import AdminSite
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.sites.admin import SiteAdmin
from django.contrib.sites.models import Site
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.forms import FlatpageForm as FlatpageFormOld

from constance.admin import Config, ConstanceAdmin

from ckeditor.widgets import CKEditorWidget

from django import forms


class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = FlatPage
        fields = '__all__'


class FlatPageAdmin(admin.ModelAdmin):
    form = FlatpageForm
    list_display = ('url', 'title')
    list_filter = ('sites', 'registration_required')
    search_fields = ('url', 'title')


class GBinAdmin(AdminSite):
    site_header = _('Gbin Admin Panel')
    site_title = _('Gbin')
    index_title = _('Admin panel')


gbin_admin = GBinAdmin(name='gbin_admin')
gbin_admin.register(User, UserAdmin)
gbin_admin.register(FlatPage, FlatPageAdmin)
gbin_admin.register(Site, SiteAdmin)
gbin_admin.register([Config], ConstanceAdmin)
gbin_admin.unregister(FlatPage)
gbin_admin.register(FlatPage, FlatPageAdmin)
