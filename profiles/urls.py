from django.conf.urls import url

from profiles.views import login_user, register_user, create_profile, \
    dashboard, logout_user, target_list, target_create, profile_edit

urlpatterns = [
    url(r'login/', login_user, name='login-user'),
    url(r'register/', register_user, name='register-user'),
    url(r'create_profile/', create_profile, name='create-profile'),
    url(r'dashboard/', dashboard, name='dashboard'),
    url(r'targets/', target_list, name='target-list'),
    url(r'target/create', target_create, name='target-create'),
    url(r'edit/$', profile_edit, name='edit-profile'),
    url(r'logout_user/', logout_user, name='logout-user'),
]
