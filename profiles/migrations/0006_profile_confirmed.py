# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2019-03-13 02:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0005_target_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='confirmed',
            field=models.BooleanField(default=False, verbose_name='confirmed'),
        ),
    ]
