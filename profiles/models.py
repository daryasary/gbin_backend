from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.urls import reverse
from django.utils import timezone

from django.utils.translation import ugettext_lazy as _

from lib.common_models import BaseModel
from utils.validators import mobile_number_validator


class Profile(BaseModel):
    """Profile class for hold additional data from user for further usages"""
    DENIED = 0
    SUSPENDED = 5
    CREATED = 10
    APPROVED = 20

    STATUS_CHOICES = (
        (DENIED, _("Denied")),
        (CREATED, _("Created")),
        (APPROVED, _("Approved"))
    )
    user = models.OneToOneField(User, verbose_name=_("user"), related_name='profile')
    status = models.PositiveSmallIntegerField(
        verbose_name=_("status"), choices=STATUS_CHOICES, default=CREATED
    )
    avatar = models.ImageField(
        upload_to='profile/avatars/', blank=True, verbose_name=_("avatar"))
    confirmed = models.BooleanField(default=False, verbose_name=_("confirmed"))

    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")

    def __str__(self):
        return self.user.username

    @property
    def mobile(self):
        """If user has phone_number associated with return it"""
        if hasattr(self, 'phone_number'):
            return self.phone_number.phone_number
        return None

    mobile.fget.short_description = _("Mobile")

    @property
    def mobile_verified(self):
        """Check if mobile_number is verified or not"""
        if hasattr(self, 'phone_number'):
            return self.phone_number.verified
        return False

    @property
    def terminal(self):
        account = self.accounts.filter(is_enable=True).first()
        if account is not None:
            return account.terminal
        return None

    @property
    def trial(self):
        return (timezone.now() - self.user.date_joined).days < 10

    @property
    def gateway(self):
        return settings.SITE_URL + reverse('donation_page', args=[self.user.username])


class ProfilePhoneNumber(BaseModel):
    """Store user phone number and validation steps in separate model to
    decrease transactions on profile model"""
    profile = models.OneToOneField(Profile, related_name='phone_number')
    phone_number = models.BigIntegerField(
        verbose_name=_('mobile number'), unique=True,
        validators=[mobile_number_validator],
        error_messages={
            'unique': _("A user with this mobile number already exists."),
        }
    )
    verified = models.BooleanField(verbose_name=_("verified"), default=False)
    verify_codes = models.CharField(
        verbose_name=_("verify codes"), blank=True, max_length=256,
        validators=[validate_comma_separated_integer_list],
    )

    class Meta:
        verbose_name = _("PhoneNumber")
        verbose_name_plural = _("PhoneNumbers")

    def __str__(self):
        return '{}: {}'.format(str(self.profile), self.phone_number)


class Target(BaseModel):
    """Store all user targets in separate models, targets have relation with
    user model and should be read for each user instead of profile"""
    NOT_STARTED = 0
    ACTIVE = 5
    SUSPENDED = 10
    UNSUCCESSFUL = 15
    SUCCESSFUL = 20
    STATUS_CHOICES = (
        (NOT_STARTED, _("Not started")),
        (ACTIVE, _("Active")),
        (SUSPENDED, _("Suspended")),
        (UNSUCCESSFUL, _("Unsuccessful")),
        (SUCCESSFUL, _("Successful"))
    )
    user = models.ForeignKey(
        User, related_name='targets', verbose_name=_("user")
    )
    profile = models.ForeignKey(
        Profile, related_name="targets", verbose_name=_("profile"), editable=False
    )
    title = models.CharField(max_length=255, verbose_name=_("title"), blank=True)
    amount = models.IntegerField(verbose_name=_("amount"))
    description = models.TextField(blank=True, verbose_name=_("description"))
    donation_message = models.TextField(
        blank=True, verbose_name=_("donation message"),
        help_text=_("the message will be shown to donors after donation")
    )
    status = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES, default=NOT_STARTED, verbose_name=_("status")
    )

    class Meta:
        verbose_name = _("Target")
        verbose_name_plural = _("Targets")

    def __str__(self):
        return '{}: {}'.format(self.user.username, self.get_status_display())

    @property
    def total_paid(self):
        return self.donations.filter(
            paid=True
        ).aggregate(total=Coalesce(Sum('amount'), 0))['total']

    @property
    def percentage(self):
        return int(self.total_paid / self.amount * 100)

    def save(self, *args, **kwargs):
        """If target with active status is submitting, here we should disable
        all other active targets"""
        if self.status == self.ACTIVE:
            Target.objects.filter(
                user=self.user, status=self.ACTIVE
            ).update(status=self.SUSPENDED)
        if self.profile is None:
            self.profile = self.user.profile
        return super().save(*args, **kwargs)


class Account(BaseModel):
    """Store bank account data, Currently just terminal_id from hesabit"""
    user = models.ForeignKey(
        User, verbose_name=_("user"), related_name='accounts'
    )
    profile = models.ForeignKey(
        Profile, related_name="accounts", verbose_name=_("profile"), editable=False
    )
    terminal = models.UUIDField(verbose_name=_("Terminal"))
    is_enable = models.BooleanField(default=True)

    class Meta:
        verbose_name = _("Target")
        verbose_name_plural = _("Targets")

    def __str__(self):
        return "{}: {}".format(self.user.username, self.is_enable)

    def save(self, *args, **kwargs):
        """Automatically save profile to the model"""
        if self.profile is None:
            self.profile = self.user.profile
        return super().save(*args, **kwargs)
