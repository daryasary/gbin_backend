from django import forms
from django.contrib.auth import get_user_model
# from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from profiles.models import Target

User = get_user_model()


class UserRegistrationForm(forms.ModelForm):
    """Create User instance form"""

    confirm_password = forms.CharField(
        widget=forms.PasswordInput, label=_("confirm password")
    )

    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'confirm_password')

    def __init__(self, *args, **kwargs):
        super(UserRegistrationForm, self).__init__(*args, **kwargs)
        if self._meta.model.USERNAME_FIELD in self.fields:
            self.fields[self._meta.model.USERNAME_FIELD].widget.attrs.update(
                {'autofocus': True})

    def clean(self):
        if self.cleaned_data['password'] != self.cleaned_data['confirm_password']:
            raise ValidationError(_("Password don't match"))
        if len(self.cleaned_data['username'])<3:
            raise ValidationError(_("Username can not be less than 3 character"))
        self.instance.username = self.cleaned_data['username']
        self.instance.email = self.cleaned_data['email']
        return super().clean()

    def save(self, commit=True):
        self.instance.username = self.cleaned_data['username']
        self.instance.email = self.cleaned_data['email']
        return super().save(commit)


class TargetCreationForm(forms.ModelForm):
    class Meta:
        model = Target
        fields = ('title', 'amount', 'description', 'donation_message')
