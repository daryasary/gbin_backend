from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.db.models import Q, Sum
from django.db.models.functions import Coalesce
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _

from lib.response_handlers import render_specific
from profiles.forms import UserRegistrationForm, TargetCreationForm
from profiles.models import Account, Profile, Target
from profiles.permissions import has_profile, has_not_profile, not_logged_in


def login_user(request):
    """Authenticate users and check if user has session in his browser
    redirect to the panel API and not continue process"""
    if request.user.is_authenticated():
        return redirect('dashboard')
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_obj = User.objects.filter(
            Q(username=username) | Q(email=username)
        ).first()
        if user_obj is None:
            messages.error(request, _("User not found"))
            return render_specific(request.user, request, 'site/login.html')
        user = authenticate(username=user_obj.username, password=password)
        if user is None:
            messages.error(request, _("Wrong credentials"))
            return render_specific(request.user, request, 'site/login.html')
        if not user.is_active:
            messages.error(request, _("User is not active"))
            return render_specific(request.user, request, 'site/login.html')
        login(request, user)
        return redirect('dashboard')

    return render_specific(request.user, request, 'site/login.html')


@user_passes_test(not_logged_in, login_url='/profile/dashboard/')
def register_user(request):
    """Create User instance and not redirect to the next step which will create
    Profile instance for user"""
    context = dict()
    if request.method == "POST":
        form = UserRegistrationForm(data=request.POST)
        if form.is_valid():
            user = form.save(commit=True)
            user.set_password(user.password)
            user.save()
            login(request, user)
            return redirect('create-profile')
        else:
            for error in form.errors.values():
                messages.error(request, error)
            context['form'] = UserRegistrationForm(initial=request.POST)
    else:
        context['form'] = UserRegistrationForm()
    return render_specific(request.user, request, 'site/register.html', context)


@login_required()
@user_passes_test(has_not_profile, login_url='/profile/dashboard/')
def create_profile(request):
    """Create Profile instance for requested user set given items to the Profile
     and Account models"""
    context = dict()
    if request.method == "POST":
        profile = Profile.objects.create(
            avatar=request.POST['avatar'], user=request.user
        )
        account = Account.objects.create(
            user=request.user, terminal=request.POST['terminal']
        )
        return redirect('dashboard')
    return render_specific(request.user, request, 'site/create_profile.html', context)


@login_required()
@user_passes_test(has_profile, login_url='/profile/create-profile/')
def confirm_email(request):
    """Check if user has confirmed his/her email or not"""
    context = dict()
    if request.user.profile.confirmed:
        return redirect('dashboard')
    # TODO: send email to the end user
    return render_specific(request.user, request, 'panel/confirm_email.html', context)


@login_required()
def logout_user(request):
    logout(request)
    return redirect('login-user')


@login_required()
@user_passes_test(has_profile, login_url='/profile/create_profile/')
def dashboard(request):
    """Dashboard panel view of streamer"""
    donations = request.user.donations.filter(paid=True)
    context = dict(
        donations=donations,
        targets=request.user.profile.targets,
        total_donations=donations.aggregate(total=Coalesce(Sum('amount'), 0))['total'],
        total_settles=0
    )
    return render_specific(request.user, request, 'panel/index.html', context)


@login_required()
@user_passes_test(has_profile, login_url='/profile/create-profile/')
def target_list(request, tid=None):
    """Complete target page wich will handle all aspects of target list,
    retrieve, create and change status operations"""
    context = dict(targets=request.user.profile.targets.all().order_by('-id'))
    if not hasattr(request.user, 'profile'):
        return redirect('create-profile')
    return render_specific(request.user, request, 'panel/target_list.html', context)


@login_required()
@user_passes_test(has_profile, login_url='/profile/create-profile/')
def target_create(request):
    """Load target form or save submitted form"""
    context = dict()
    if request.method == "POST":
        form = TargetCreationForm(data=request.POST)
        if form.is_valid():
            target = form.save(commit=False)
            target.user = request.user
            target.profile = request.user.profile
            target.status = Target.ACTIVE if request.POST.get('status') == 'on' else Target.NOT_STARTED
            target.save()
            return redirect('target-list')
    context['form'] = TargetCreationForm()
    return render_specific(request.user, request, 'panel/target_create.html', context)


@login_required()
@user_passes_test(has_profile, login_url='profile/create-profile')
def profile_edit(request):
    """Provide ability to users to edit profile info or bank account details,
    initial values is empty at creation time, and is accessible even when
    profile trial version is ended"""
    context = dict()
    # TODO: Render profile form + initial data
    # TODO: Render account form + initial data
    return render_specific(request.user, request, 'panel/profile_edit.html', context)
