def has_profile(user):
    return hasattr(user, 'profile')


def has_not_profile(user):
    return not hasattr(user, 'profile')


def not_logged_in(user):
    return not user.is_authenticated()
