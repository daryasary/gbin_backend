from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from gbin.admin import gbin_admin
from lib.common_admin import BaseAdmin
from profiles.models import ProfilePhoneNumber, Profile, Target, Account


class PhoneNumberInline(admin.TabularInline):
    model = ProfilePhoneNumber


class TargetInline(admin.TabularInline):
    model = Target
    extra = 1


class AccountInline(admin.TabularInline):
    model = Account
    extra = 1


class ProfileAdmin(BaseAdmin):
    extra_list_display = ['image', 'user', 'status', 'mobile', 'verified']
    search_fields = ['user__username', 'mobile_verified']
    list_filter = ['status']
    inlines = (PhoneNumberInline, TargetInline, AccountInline)

    def image(self, obj):
        try:
            path = obj.avatar.url
        except:
            path = '/static/appearances/default/site/image/55.jpg'
        return format_html('<span><image src={} style="width: 30px;"></span>'.format(path))
    image.short_description = _("avatar")

    def verified(self, obj):
        return obj.mobile_verified
    verified.boolean = True
    verified.short_description = _("verified")


gbin_admin.register(Profile, ProfileAdmin)
