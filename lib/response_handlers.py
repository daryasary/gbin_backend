from django.shortcuts import render

from utils.appearances import get_complete_template_path_for_user


def render_specific(user, request, template_key, *args, **kwargs):
    """Override django default render function, get one extra arg called user,
    find user preferred user and load custom template for each user, if user
    has no custom template return default template"""
    template_name = get_complete_template_path_for_user(user, template_key)
    return render(request, template_name, *args, **kwargs)


