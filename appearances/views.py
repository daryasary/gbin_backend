from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

from lib.response_handlers import render_specific
from transactions.forms import DonationRequestForm
from utils.appearances import get_complete_template_path_for_user


class HomeView(TemplateView):
    template = 'site/index.html'

    @property
    def template_name(self):
        return get_complete_template_path_for_user(self.request.user, self.template)


class WidgetView(TemplateView):
    template = 'panel/widget.html'

    @property
    def template_name(self):
        return get_complete_template_path_for_user(self.request.user, self.template)


def donation_page(request, username):
    """If request id GET render form and if request id POST redirect to bank"""
    user = get_object_or_404(User, **{'username': username})
    target = user.targets.filter(status=5).last()
    if not user.accounts.filter(is_enable=True).exists() and not user.profile.trial:
        messages.error(request, _("User has not valid terminal"))
    if request.method == 'POST':
        form = DonationRequestForm(data=request.POST)
        if form.is_valid():
            donation = form.save(commit=False)
            donation.user = user
            if target is not None:
                donation.target = target
            donation.save()
            response = donation.make_request()
            if response is not None:
                return redirect(response['link'])
            else:
                messages.error(
                    request, _("There is a problem, please try again later")
                )
    context = dict(
        user=user, target=target, form=DonationRequestForm(),
    )
    return render_specific(user, request, 'site/donation_page.html', context)
