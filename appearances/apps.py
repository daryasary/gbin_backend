from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AppearancesConfig(AppConfig):
    name = 'appearances'
    verbose_name = _("appearance")
    verbose_name_plural = _("appearances")
