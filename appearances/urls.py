from django.conf.urls import url, include
from appearances.views import HomeView, donation_page, WidgetView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'profile/widgets/$', WidgetView.as_view(), name='widget'),
    url(r'p/', include('django.contrib.flatpages.urls')),
    url(r'(?P<username>.*)/$', donation_page, name='donation_page')
]
