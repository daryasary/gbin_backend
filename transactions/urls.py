from django.conf.urls import url

from transactions.views import confirm_payments

urlpatterns = [
    url(r'verify_payment/$', confirm_payments, name='verify_payment'),
]
