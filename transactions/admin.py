from django.utils.translation import ugettext_lazy as _

from transactions.models import DonationRequest
from gbin.admin import gbin_admin
from lib.common_admin import BaseAdmin


class DonationRequestAdmin(BaseAdmin):
    extra_list_display = ['user', 'amount', 'paid', 'donor_name', 'desc']
    list_filter = ['paid']
    search_fields = ['donor_name', 'user__username']

    def desc(self, obj):
        return ' '.join(obj.donor_description.split(' ')[:30]) + '...'

    desc.short_description = _('desc')


gbin_admin.register(DonationRequest, DonationRequestAdmin)
