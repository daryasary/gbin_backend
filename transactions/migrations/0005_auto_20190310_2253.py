# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2019-03-10 19:23
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('transactions', '0004_auto_20190310_2250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donationrequest',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, verbose_name='donate uuid'),
        ),
    ]
