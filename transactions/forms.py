from django import forms

from transactions.models import DonationRequest


class DonationRequestForm(forms.ModelForm):

    class Meta:
        model = DonationRequest
        fields = (
            'amount', 'donor_name', 'donor_description', 'donor_phone_number',
            'donor_email', 'stream_credentials', 'stream_description',
            'list_credentials'
        )
