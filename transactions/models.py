import uuid
import requests
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from lib.common_models import BaseModel
from profiles.models import Target
from utils.validators import mobile_number_validator


class DonationRequest(BaseModel):
    user = models.ForeignKey(
        User, related_name='donations', verbose_name=_("user")
    )
    target = models.ForeignKey(
        Target, related_name='donations', verbose_name=_("target"),
        blank=True, null=True
    )
    amount = models.IntegerField(verbose_name=_("amount"))
    paid = models.BooleanField(default=False, verbose_name=_("paid"))
    donor_name = models.CharField(
        verbose_name=_("donor name"), max_length=256, blank=True
    )
    donor_description = models.TextField(blank=True)
    donor_phone_number = models.BigIntegerField(
        verbose_name=_('donor mobile number'), blank=True, null=True,
        validators=[mobile_number_validator],
        error_messages={
            'unique': _("A user with this mobile number already exists."),
        }
    )
    uuid = models.UUIDField(verbose_name=_("donate uuid"), default=uuid.uuid4)
    donor_email = models.EmailField(
        verbose_name=_("donor email"), null=True, blank=True
    )
    stream_credentials = models.BooleanField(
        default=True, verbose_name=_("Stream credentials")
    )
    stream_description = models.BooleanField(
        default=True, verbose_name=_("Stream description")
    )
    list_credentials = models.BooleanField(
        default=True, verbose_name=_("List credentials")
    )

    payment_token = models.CharField(
        verbose_name=_("payment token"), blank=True, max_length=32
    )
    gateway_logs = models.TextField(blank=True)
    payment_logs = models.TextField(blank=True)

    class Meta:
        verbose_name = _("DonationRequest")
        verbose_name_plural = _("DonationRequests")

    def __str__(self):
        return '{}: {}'.format(self.user.username, self.amount)

    def payload(self):
        """Generate hesabit preferred data"""
        data = {
            "amount": self.amount,
            "redirect_url": settings.SITE_URL + reverse('home'),
            "order_id": self.uuid
        }
        if self.user.profile.terminal is None:
            if self.user.profile.trial():
                data['terminal'] = settings.DEFAULT_TERMINAL_ID
            else:
                raise Exception('User trial usage is finished')
        else:
            data['terminal'] = self.user.profile.terminal
        if self.donor_email:
            data['email'] = self.donor_email
        if self.donor_email:
            data['mobile'] = self.donor_phone_number
        return data

    def make_request(self):
        """Send standard request to the hesabit api and save response"""
        try:
            data = self.payload()
        except:
            return None
        result = requests.post("https://pay.hesabit.com/start", data=data)
        self.gateway_logs += result.text
        if result.status_code != requests.codes.ok:
            res = None
        else:
            res = result.json()
            self.payment_token = res.get('token', '')
        self.save()
        return res

    def verify_payment(self):
        if self.user.profile.terminal is None:
            data = {
                "terminal": settings.DEFAULT_TERMINAL_ID,
                "token": self.payment_token
            }
        else:
            data = {
                "terminal": self.user.profile.terminal,
                "token": self.payment_token
            }
        result = requests.post("https://pay.hesabit.com/verify", data=data)
        if result.status_code != requests.codes.ok:
            return
        self.paid = (int(result['status']) == 1) and (int(result['amount'] == self.amount))
        self.save()
