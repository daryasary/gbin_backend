from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from transactions.models import DonationRequest


def confirm_payments(request):
    if request.method == 'POST':
        try:
            donation_request = DonationRequest.objects.get(
                payment_token=request.POST['token']
            )
        except:
            donation_request = get_object_or_404(
                DonationRequest, uuid=request.POST['order_id']
            )
        donation_request.payment_logs = request.POST
        donation_request.save()
        if request.POST['status'] > 0:
            donation_request.verify_payment()
    return HttpResponse('Processed')
